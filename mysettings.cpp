#include "mysettings.h"

MySettings::MySettings(){

    settings = new QSettings("./Config/treeConfig.ini",QSettings::IniFormat);
}

MySettings::~MySettings(){
}

void MySettings::showDialog()
{
    QDialog settingDialog;
    settingDialog.setLayout(new QVBoxLayout());
    QTabWidget settingTab(&settingDialog);

//Global options
    QTableView globalTab (&settingDialog);
    QFormLayout globalLayout;

    QSpinBox fontSizeSpin;
    fontSizeSpin.setMinimum(FONT_SIZE_MIN);
    fontSizeSpin.setValue(getFontSize());
    globalLayout.addRow("Font size",&fontSizeSpin);

    globalTab.setLayout(&globalLayout);
    settingTab.addTab(&globalTab,"Global");

//Tree options
    QTableView treeTab (&settingDialog);
    QFormLayout treeLayout(&treeTab);

    QSpinBox branchWidthSpin;
    branchWidthSpin.setMinimum(BRANCH_WIDTH_MIN);
    branchWidthSpin.setValue(int(getBranchWidth()));
    treeLayout.addRow("Branch width",&branchWidthSpin);

    QCheckBox autoHeight;
    autoHeight.setChecked(int(isEnableAutoTreeHeight()));
    treeLayout.addRow("Auto tree height",&autoHeight);

    QSpinBox treeHeightSpin;
    treeHeightSpin.setRange(TREE_SIZE_MIN,TREE_SIZE_MAX);
    treeHeightSpin.setValue(int(getTreeHeight()));
    treeHeightSpin.setSingleStep(TREE_SIZE_STEP);
    treeLayout.addRow("Default tree height",&treeHeightSpin);
    treeHeightSpin.setEnabled(!autoHeight.isChecked());

    QSpinBox treeWidthSpin;
    treeWidthSpin.setRange(TREE_SIZE_MIN,TREE_SIZE_MAX);
    treeWidthSpin.setValue(int(getTreeWidth()));
    treeWidthSpin.setSingleStep(TREE_SIZE_STEP);
    treeLayout.addRow("Default tree width",&treeWidthSpin);

    QDoubleSpinBox minBootstrapSpin;
    minBootstrapSpin.setRange(BOOTSTRAP_MIN,BOOTSTRAP_MAX);
    minBootstrapSpin.setSingleStep(BOOTSTRAP_STEP);
    minBootstrapSpin.setValue(getMinBootstrap());
    treeLayout.addRow("Minimum bootstrap visible",&minBootstrapSpin);

    QComboBox treeStyleCB;
    treeStyleCB.addItem("Phylogram",QString("phylogram"));
    treeStyleCB.addItem("Cladogram",QString("cladogram"));
    treeLayout.addRow("Tree style",&treeStyleCB);

    QObject::connect(&autoHeight, &QCheckBox::stateChanged, &treeHeightSpin, &QSpinBox::setDisabled);

    treeTab.setLayout(&treeLayout);
    settingTab.addTab(&treeTab,"Tree");

//Taxon color settings
    QTableView taxonTab (&settingDialog);
    QGridLayout taxonLayout(&taxonTab);

    taxonTree = new QTreeWidget(&taxonTab);
//    @TODO remplissage de l'arbre taxonomique
//    fillTaxonTree();
    taxonLayout.addWidget(taxonTree,0,0,4,4);

    QPushButton customColor("custom color...");
    connect(&customColor,SIGNAL(clicked()),this,SLOT(customColor_clicked()));
    taxonLayout.addWidget(&customColor,6,0,1,4);

    taxonTab.setLayout(&taxonLayout);
    settingTab.addTab(&taxonTab,"Taxon");

    settingDialog.layout()->addWidget(&settingTab);

    QDialogButtonBox buttonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    buttonBox.connect(&buttonBox, &QDialogButtonBox::accepted, &settingDialog, &QDialog::accept);
    buttonBox.connect(&buttonBox, &QDialogButtonBox::rejected, &settingDialog, &QDialog::reject);

    settingDialog.layout()->addWidget(&buttonBox);
    settingDialog.setFixedSize(600,450);
    settingDialog.exec();

    if(settingDialog.result()){

        setFontSize(fontSizeSpin.value());

        setBranchWidth(unsigned(branchWidthSpin.value()));
        enableAutoTreeHeight(autoHeight.isChecked());
        setTreeWidth(unsigned(treeWidthSpin.value()));
        setTreeHeight(unsigned(treeHeightSpin.value()));
        setMinBootstrap(minBootstrapSpin.value());
        setTreeStyle(treeStyleCB.currentData().toString());
    }
}

void MySettings::customColor_clicked(){
    if(!taxonTree->currentItem())
        return;

    QColorDialog colorDial;
    QColor color = colorDial.getRgba();

    settings->beginGroup("taxonColor");
    settings->setValue(taxonTree->currentItem()->text(0),color);
    settings->endGroup();

    taxonTree->currentItem()->setBackgroundColor(0,color);
    taxonTree->repaint();
}

void MySettings::fillTaxonTree(){
    ifstream nodeFile("Config/nodes.lst");
    ifstream namesToSetsFile("Config/names_sets.lst");
    ifstream myColorsFile("Config/mycolors.lst");

    if(!nodeFile.is_open() || !namesToSetsFile.is_open() || !myColorsFile.is_open()){
        cout << "Error opening files" << endl;
        return;
    }

    char currData[100];
    while(!nodeFile.eof()){
        nodeFile.getline(currData,100);
    }

}
