#include "mytreeseq.h"

MyTreeSeq::MyTreeSeq(QWidget* parent, vector<Tree * > treeList,VectorSequenceContainer* seqList):
QWidget(parent){

    this->treeList = treeList;
    this->seqList = seqList;

    myTree = new MyTree(nullptr,treeList);
    treeCanvas = myTree->openInCanvas();
    mySeq = new MySeq(seqList,treeCanvas,this);

    connect(treeCanvas,SIGNAL(hasChanged()),mySeq,SLOT(reload_seq()));
}

void MyTreeSeq::openInWindow(){
    window = new SecondaryWindow(this);
    setupDispay();
    window->setCentralWidget(display);
    window->show();

}

void MyTreeSeq::setupDispay(){
    display =new QWidget(window);
    display->setLayout(new QHBoxLayout(display));

    treeCanvas->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    treeCanvas->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    treeCanvas->enableWheelEvent(false);

    display->layout()->addWidget(treeCanvas);
    display->layout()->addWidget(mySeq->getSeqGraphicsView());

    connect(this,SIGNAL(pass_add_color()),mySeq,SLOT(add_color()));
}
