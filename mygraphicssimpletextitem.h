#ifndef MYGRAPHICSSIMPLETEXTITEM_H
#define MYGRAPHICSSIMPLETEXTITEM_H

#include <iostream>

#include "mycolor.h"
#include "mysettings.h"

#include <QGraphicsSimpleTextItem>
#include <QString>
#include <QPainter>
#include <QStyleOptionGraphicsItem>

using namespace std;

/**
 * @brief The MyGraphicsSimpleTextItem class allow colored text
 */
class MyGraphicsSimpleTextItem : public QGraphicsSimpleTextItem
{

public:
    MyGraphicsSimpleTextItem(QString text,bool showColor=false,string alphabetType="",QPoint pos=QPoint(0,0));
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    unsigned long getPosX(){return posX;}
    unsigned long getPosY(){return posY;}

    void setColorTool(MyColor * color){this->colorTool = color;}

private:
    bool showColor;
    MySettings settings;
    string alphabetType;
    MyColor * colorTool;
    unsigned long posX;//position dans la sequence
    unsigned long posY;//numero de la sequence a laquelle il apartient
};

#endif // MYGRAPHICSSIMPLETEXTITEM_H
