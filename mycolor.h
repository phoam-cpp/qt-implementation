#ifndef MYCOLOR_H
#define MYCOLOR_H

#include <map>

#include <QString>
#include <QColor>

using namespace std;


/**
 * @brief The MyColor class provides map objects with defaults color for differents sequences types
 */
class MyColor
{
public:
    MyColor();
    ~MyColor();

    map<QString,QColor> nuclToColor;

    map<QString,QColor> aminoToColor;

};

#endif // MYCOLOR_H
