#include "mygraphicssimpletextitem.h"

MyGraphicsSimpleTextItem::MyGraphicsSimpleTextItem(QString text,bool showColor,string alphabetType,QPoint pos):
QGraphicsSimpleTextItem(text){
    this->showColor=showColor;
    this->alphabetType=alphabetType;
    this->posX=unsigned(long(pos.x()));
    this->posY=unsigned(long(pos.y()));
}

void MyGraphicsSimpleTextItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QStyleOptionGraphicsItem* style = const_cast<QStyleOptionGraphicsItem*>(option);
    if(showColor){
        if(this->alphabetType=="DNA" || this->alphabetType=="RNA")
            painter->fillRect(option->rect,colorTool->nuclToColor[this->text()]);
        else if(this->alphabetType=="Proteic") {
            painter->fillRect(option->rect,colorTool->aminoToColor[this->text()]);
        }
        else{
            painter->fillRect(option->rect,settings.getTaxonColor());
        }
    }
    QGraphicsSimpleTextItem::paint(painter, style, widget);
}
