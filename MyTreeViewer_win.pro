#-------------------------------------------------
#
# Project created by QtCreator 2019-04-15T09:50:16
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MyTreeViewer
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp \
        mycolor.cpp \
        mygraphicssimpletextitem.cpp \
        mygraphicsview.cpp \
        myseq.cpp \
        mysettings.cpp \
        mytree.cpp \
        mytreecanvas.cpp \
        mytreedrawinglistener.cpp \
        mytreeseq.cpp \
        secondarywindow.cpp

HEADERS += \
        mainwindow.h \
        mycolor.h \
        mygraphicssimpletextitem.h \
        mygraphicsview.h \
        myseq.h \
        mysettings.h \
        mytree.h \
        mytreecanvas.h \
        mytreedrawinglistener.h \
        mytreeseq.h \
        secondarywindow.h

FORMS += \
        mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target


win32: LIBS += -L$$PWD/../../Bpp/build/MinGW64_Qt/lib/ -llibbpp-core.dll
win32: LIBS += -L$$PWD/../../Bpp/build/MinGW64_Qt/lib/ -llibbpp-seq.dll
win32: LIBS += -L$$PWD/../../Bpp/build/MinGW64_Qt/lib/ -llibbpp-phyl.dll
win32: LIBS += -L$$PWD/../../Bpp/build/MinGW64_Qt/lib/ -llibbpp-qt.dll


INCLUDEPATH += $$PWD/../../Bpp/build/MinGW64_Qt/include
DEPENDPATH += $$PWD/../../Bpp/build/MinGW64_Qt/include
