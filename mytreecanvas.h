#ifndef MYTREECANVAS_H
#define MYTREECANVAS_H

#include <QMainWindow>
#include <QMenu>
#include <QColorDialog>

#include "Bpp/Qt/Tree/TreeCanvas.h"
#include "Bpp/Qt/Tree/TreeCanvasControlers.h"
#include "Bpp/Qt/Tree/TreeStatisticsBox.h"
#include "Bpp/Phyl/TreeTemplate.h"

#include "mysettings.h"
#include "mygraphicssimpletextitem.h"

using namespace std;
using namespace bpp;

#define SWAP 1
#define COLLAPSE 2
#define REROOT 3
#define RGB_BLACK RGBColor(0,0,0)
#define RGB_RED RGBColor(230,50,50)
#define RGB_GREEN RGBColor(50,230,50)
#define RGB_BLUE RGBColor(50,50,230)

/**
 * @brief The MyTreeCanvas class implement TreeCanvas and TreeCanvasControlerListener, handle user's input and tree modification
 */
class MyTreeCanvas : public TreeCanvas, public TreeCanvasControlersListener
{

    Q_OBJECT //requis pour utiliser connect(sender,signal,receiver,slot)

public:
    explicit MyTreeCanvas(QWidget * parent=nullptr);
    ~MyTreeCanvas()override;
    /**
     * @brief getStat return a box with statistics about the tree
     * @return the statistics box given by Bpp
     */
    TreeStatisticsBox * getStat(void) {return tSB;}
    /**
     * @brief getControl return an object that gives some widgets to modify the display/display of information
     * @return the canvas controler object
     */
    TreeCanvasControlers * getControl(void) {return tCC;}
    int getSelectedNode(void){return selectedNode;}
    void showContextMenu(bool,QPoint);
    void enableWheelEvent(bool b){wheelEventEnable=b;}

    void controlerTakesAction()override;
    void wheelEvent(QWheelEvent *event)override;
    void keyPressEvent(QKeyEvent*)override;
    void keyReleaseEvent(QKeyEvent * event)override;
    void setTree(const Tree* tree)override;

    /**
     * @brief changeInTree handle change like swap, root, ...
     */
    void changeInTree(int);
    /**
     * @brief changeColor change the color propertie of a node and his childs(call changeChildsColor)
     */
    void changeColor(RGBColor);
    void changeChildsColor(int nodeId, RGBColor color);

    void setPointSize(unsigned int i){pointSize=i;}
    unsigned int getPointSize(){return pointSize;}

    void setZoomH(unsigned int h){zoomHeight=h;}
    unsigned int getZoomH(){return zoomHeight;}

    void setZoomW(unsigned int w){zoomWidth=w;}
    unsigned int getZoomW(){return zoomWidth;}

    void setShowBootstrap(bool b){showBootstrap=b;}
    bool getShowBootstrap(){return showBootstrap;}

    void mousePressEvent(QMouseEvent * event)override;


private:
    MySettings settings;

    QWidget * parent;

    int selectedNode;

    TreeTemplate< Node > * tree;

    TreeCanvasControlers * tCC=nullptr;//fournit les controls d'affichage du canvas (widgets fournis par Bio++)
    TreeStatisticsBox * tSB=nullptr;//widget avec des infos sur l'arbre (taille, nfurcation ...)

    unsigned int pointSize=settings.getBranchWidth();
    unsigned int zoomHeight=settings.getTreeHeight();
    unsigned int zoomWidth=settings.getTreeWidth();
    bool showBootstrap = false;

    bool wheelEventEnable=true;
    bool ctrlPressed =false;
    bool altPressed=false;

private: signals:
    void rightClicked(NodeMouseEvent *);
    void nextTree();
    void previousTree();
    void lastTree();
    void firstTree();
    void hasChanged();

private slots:
    void swap_triggered(){changeInTree(SWAP);}
    void collapse_triggered(){changeInTree(COLLAPSE);}
    void reroot_triggered(){changeInTree(REROOT);}

    void showBootstrap_triggered(){setShowBootstrap(!showBootstrap);changeInTree(0);}

    void increaseWidth_triggered(){setPointSize(getPointSize()+1);redraw();}
    void decreaseWidth_triggered(){if(getPointSize()-1>0)setPointSize(getPointSize()-1);redraw();}

    void colorBlack_triggered(){changeColor(RGB_BLACK);}
    void colorCustom_triggered(){QColorDialog qdc; QColor color = qdc.getColor(); changeColor(RGBColor(color.red(),color.green(),color.blue()));}
    void colorRed_triggered(){changeColor(RGB_RED);}
    void colorGreen_triggered(){changeColor(RGB_GREEN);}
    void colorBlue_triggered(){changeColor(RGB_BLUE);}

};

#endif // MYTREECANVAS_H
