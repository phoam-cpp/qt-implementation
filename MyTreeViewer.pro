#-------------------------------------------------
#
# Project created by QtCreator 2019-04-15T09:50:16
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MyTreeViewer
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp \
        mycolor.cpp \
        mygraphicssimpletextitem.cpp \
        mygraphicsview.cpp \
        myseq.cpp \
        mysettings.cpp \
        mytree.cpp \
        mytreecanvas.cpp \
        mytreedrawinglistener.cpp \
        mytreeseq.cpp \
        secondarywindow.cpp

HEADERS += \
        mainwindow.h \
        mycolor.h \
        mygraphicssimpletextitem.h \
        mygraphicsview.h \
        myseq.h \
        mysettings.h \
        mytree.h \
        mytreecanvas.h \
        mytreedrawinglistener.h \
        mytreeseq.h \
        secondarywindow.h

FORMS += \
        mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../usr/lib/libbpp/release/ -lbpp-core
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../usr/lib/libbpp/debug/ -lbpp-core
else:unix: LIBS += -L$$PWD/../../../../../usr/lib/libbpp/ -lbpp-core

INCLUDEPATH += $$PWD/../../../../../usr/include/Bpp
DEPENDPATH += $$PWD/../../../../../usr/include/Bpp

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/lib/libbpp/release/libbpp-core.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/lib/libbpp/debug/libbpp-core.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/lib/libbpp/release/bpp-core.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/lib/libbpp/debug/bpp-core.lib
else:unix: PRE_TARGETDEPS += $$PWD/../../../../../usr/lib/libbpp/libbpp-core.a

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../usr/lib/libbpp/release/ -lbpp-seq
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../usr/lib/libbpp/debug/ -lbpp-seq
else:unix: LIBS += -L$$PWD/../../../../../usr/lib/libbpp/ -lbpp-seq

INCLUDEPATH += $$PWD/../../../../../usr/include/Bpp/Seq
DEPENDPATH += $$PWD/../../../../../usr/include/Bpp/Seq

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/lib/libbpp/release/libbpp-seq.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/lib/libbpp/debug/libbpp-seq.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/lib/libbpp/release/bpp-seq.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/lib/libbpp/debug/bpp-seq.lib
else:unix: PRE_TARGETDEPS += $$PWD/../../../../../usr/lib/libbpp/libbpp-seq.a

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../usr/lib/libbpp/release/ -lbpp-phyl
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../usr/lib/libbpp/debug/ -lbpp-phyl
else:unix: LIBS += -L$$PWD/../../../../../usr/lib/libbpp/ -lbpp-phyl

INCLUDEPATH += $$PWD/../../../../../usr/include/Bpp/Phyl
DEPENDPATH += $$PWD/../../../../../usr/include/Bpp/Phyl

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/lib/libbpp/release/libbpp-phyl.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/lib/libbpp/debug/libbpp-phyl.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/lib/libbpp/release/bpp-phyl.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/lib/libbpp/debug/bpp-phyl.lib
else:unix: PRE_TARGETDEPS += $$PWD/../../../../../usr/lib/libbpp/libbpp-phyl.a

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../usr/lib/libbpp/release/ -lbpp-qt
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../usr/lib/libbpp/debug/ -lbpp-qt
else:unix: LIBS += -L$$PWD/../../../../../usr/lib/libbpp/ -lbpp-qt

INCLUDEPATH += $$PWD/../../../../../usr/include/Bpp/Qt
DEPENDPATH += $$PWD/../../../../../usr/include/Bpp/Qt

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/lib/libbpp/release/libbpp-qt.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/lib/libbpp/debug/libbpp-qt.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/lib/libbpp/release/bpp-qt.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/lib/libbpp/debug/bpp-qt.lib
else:unix: PRE_TARGETDEPS += $$PWD/../../../../../usr/lib/libbpp/libbpp-qt.a
