#include "mygraphicsview.h"

MyGraphicsView::MyGraphicsView(QWidget * parent,string alphabet):
QGraphicsView(parent){
    this->alphabet = alphabet;
    connect(this,SIGNAL(delete_item(MyGraphicsSimpleTextItem*)),parent,SLOT(remove_seq(MyGraphicsSimpleTextItem*)));
}

MyGraphicsView::~MyGraphicsView(){

}

void MyGraphicsView::mousePressEvent(QMouseEvent *event){
    QGraphicsItem* item = this->itemAt(event->pos());
    if(!item)
        return;

    textItem = dynamic_cast<MyGraphicsSimpleTextItem*>(item);
    QGraphicsView::mousePressEvent(event);
}

void MyGraphicsView::keyPressEvent(QKeyEvent *event){
    if(!textItem){
        QGraphicsView::keyPressEvent(event);
        return;
    }

    if(event->key()==Qt::Key_Delete){
        emit delete_item(textItem);
    }

    if(event->key()==Qt::Key_Up){
        if(int(textItem->getPosY())-1<0)    return;

        gSeqList->at(textItem->getPosY())->at(textItem->getPosX()+1)->setSelected(false);

        if(gSeqList->at(textItem->getPosY()-1)->size() <= textItem->getPosX()+1){
            gSeqList->at(textItem->getPosY()-1)->at(gSeqList->at(textItem->getPosY()-1)->size()-1)->setSelected(true);
            textItem=dynamic_cast<MyGraphicsSimpleTextItem*>(gSeqList->at(textItem->getPosY()-1)->at(gSeqList->at(textItem->getPosY()-1)->size()-1));
        }else{
            gSeqList->at(textItem->getPosY()-1)->at(textItem->getPosX()+1)->setSelected(true);
            textItem=dynamic_cast<MyGraphicsSimpleTextItem*>( gSeqList->at(textItem->getPosY()-1)->at(textItem->getPosX()+1));
        }

        return;
    }
    if(event->key()==Qt::Key_Right){
        if(textItem->getPosX()+2>gSeqList->at(textItem->getPosY())->size()-1)    return;
        gSeqList->at(textItem->getPosY())->at(textItem->getPosX()+2)->setSelected(true);
        gSeqList->at(textItem->getPosY())->at(textItem->getPosX()+1)->setSelected(false);
        textItem=dynamic_cast<MyGraphicsSimpleTextItem*>( gSeqList->at(textItem->getPosY())->at(textItem->getPosX()+2));
        return;
    }
    if(event->key()==Qt::Key_Left && textItem->getPosX() != 0){
        if(int(textItem->getPosX())<0)    return;
        gSeqList->at(textItem->getPosY())->at(textItem->getPosX())->setSelected(true);
        gSeqList->at(textItem->getPosY())->at(textItem->getPosX()+1)->setSelected(false);
        textItem=dynamic_cast<MyGraphicsSimpleTextItem*>( gSeqList->at(textItem->getPosY())->at(textItem->getPosX()));
        return;
    }
    if(event->key()==Qt::Key_Down){
        if(textItem->getPosY()+1>gSeqList->size()-1)    return;

        gSeqList->at(textItem->getPosY())->at(textItem->getPosX()+1)->setSelected(false);

        if(gSeqList->at(textItem->getPosY()+1)->size()<=textItem->getPosX()+2){
            gSeqList->at(textItem->getPosY()+1)->at(gSeqList->at(textItem->getPosY()+1)->size()-1)->setSelected(true);
            textItem=dynamic_cast<MyGraphicsSimpleTextItem*>(gSeqList->at(textItem->getPosY()+1)->at(gSeqList->at(textItem->getPosY()+1)->size()-1));
        }else{
            gSeqList->at(textItem->getPosY()+1)->at(textItem->getPosX()+1)->setSelected(true);
            textItem=dynamic_cast<MyGraphicsSimpleTextItem*>( gSeqList->at(textItem->getPosY()+1)->at(textItem->getPosX()+1));

        }
        return;
    }

    if(alphabet=="DNA"){
        vector<char> vect{'A','T','G','C','-'};
        allowedChar=vect;
    }
    else if (alphabet=="RNA") {
        vector<char> vect{'A','U','G','C','-'};
        allowedChar=vect;
    }
    else {
        vector<char> vect{'A','C','D','E','F','G','H','I','K','L','M','N','P','Q','R','S','T','V','W','Y','-'};
        allowedChar=vect;
    }
    if (std::find(allowedChar.begin(), allowedChar.end(), event->text().toUpper().toStdString()[0]) != allowedChar.end())
    {
      textItem->setText(event->text().toUpper());
    }

    emit has_changed();

    QGraphicsView::keyPressEvent(event);
}
