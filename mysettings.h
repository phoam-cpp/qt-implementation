#ifndef SETTINGS_H
#define SETTINGS_H

#include <fstream>
#include <string>
#include <iostream>
#include <map>

#include "QGraphicsSimpleTextItem"

#include <QDialog>
#include <QLayout>
#include <QTabWidget>
#include <QTableView>
#include <QFormLayout>
#include <QSpinBox>
#include <QCheckBox>
#include <QDialogButtonBox>
#include <QSettings>
#include <QComboBox>
#include <QTreeWidget>
#include <QPushButton>
#include <QLabel>
#include <QColorDialog>

using namespace std;

#define TREE_SIZE_MIN 100
#define TREE_SIZE_MAX 50000
#define TREE_SIZE_STEP 10

#define BRANCH_WIDTH_MIN 1

#define AUTO_HEIGHT_DEFAULT 1

#define BOOTSTRAP_MIN 0
#define BOOTSTRAP_MAX 1
#define BOOTSTRAP_STEP 0.05

#define FONT_SIZE_DEFAULT 12
#define FONT_SIZE_MIN 1

#define TREE_STYLE_DEFAULT "phylogram"

#define TAXON_COLOR_DEFAULT QColor(Qt::white)

/**
* @brief provide current settings or an interface to modify them
*/
class MySettings : public QWidget
{
    Q_OBJECT

public:
    MySettings();
    ~MySettings();
    /**
     * @brief build and show the settings panel
     */
    void showDialog();
    void fillTaxonTree();

    unsigned int getBranchWidth(){return settings->value("branchWidth",BRANCH_WIDTH_MIN).toUInt();}
    unsigned int getTreeWidth(){return settings->value("treeWidth",TREE_SIZE_MIN).toUInt();}
    unsigned int getTreeHeight(){return settings->value("treeHeight",TREE_SIZE_MIN).toUInt();}
    bool isEnableAutoTreeHeight(){return settings->value("autotreeHeight",AUTO_HEIGHT_DEFAULT).toBool();}
    double getMinBootstrap(){return settings->value("minBootstrap",BOOTSTRAP_MIN).toDouble();}
    int getFontSize(){return settings->value("fontSize",FONT_SIZE_DEFAULT).toInt();}
    QRectF getBoundingBox(){QGraphicsSimpleTextItem i("I");i.setFont(QFont("Courier",getFontSize())); return i.boundingRect();}
    QString getTreeStyle(){return settings->value("treeStyle",TREE_STYLE_DEFAULT).value<QString>();}
    QColor getTaxonColor(){return settings->value("taxonColor",TAXON_COLOR_DEFAULT).value<QColor>();}

    void setBranchWidth(unsigned int w){settings->setValue("branchWidth",w);}
    void setTreeWidth(unsigned int w){settings->setValue("treeWidth", w);}
    void setTreeHeight(unsigned int w){settings->setValue("treeHeight", w);}
    void enableAutoTreeHeight(bool b){settings->setValue("autotreeHeight", b);}
    void setMinBootstrap(double b){settings->setValue("minBootstrap", b);}
    void setFontSize(int i){settings->setValue("fontSize",i);}
    void setTreeStyle(QString s){settings->setValue("treeStyle",s);}
    void setTaxonColor(QColor c){settings->setValue("taxonColor",c);}

private:
    QSettings * settings;

    QTreeWidget * taxonTree;

private slots:
    void customColor_clicked();

};

#endif // SETTINGS_H
