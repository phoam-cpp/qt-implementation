#ifndef MYTREE_H
#define MYTREE_H

#include <iostream>

#include "mytreecanvas.h"
#include "mytreedrawinglistener.h"
#include "secondarywindow.h"


#include <QHBoxLayout>
#include <QScrollBar>
#include <QGraphicsItem>
#include <QWidget>

#include "Bpp/Phyl/TreeTemplate.h"
#include "Bpp/Phyl/Io/Newick.h"
#include "Bpp/Qt/QtGraphicDevice.h"
#include "Bpp/Qt/QtTools.h"

using namespace std;
using namespace bpp;

/**
 * @brief The MyTree class provides tools to display a tree and navigate through a list.
 */
class MyTree: public QWidget
{
    Q_OBJECT

public:
    MyTree(QWidget*,vector<Tree * >);
    ~MyTree();
    /**
     * @brief openInWindow open the tree directly in a canvas in a window
     */
    void openInWindow();
    /**
     * @brief openInCanvas open a tree in a canvas without window
     * @return return the canvas where the tree is drawn
     */
    MyTreeCanvas* openInCanvas();
    void setupWindow();
    void loadTree(unsigned long index);
    /**
     * @brief setTreeIndex change the current index and load the corresponding tree in the canvas
     * @param index the new index
     */
    void setTreeIndex(unsigned long index);
    void setAutoTreeSize();



private:
    MySettings settings;
    QWidget* parent=nullptr;

    vector<Tree * > treeList;
    unsigned long treeIndex=0;
    TreeTemplate< Node > * tree=nullptr;

    SecondaryWindow * window=nullptr;
    MyTreeCanvas * canvas=nullptr;
    MyTreeDrawingListener* drawingListener=nullptr;
private: signals:
    void signal_delete_instance(QWidget*);
    void haschanged();

private slots:
    void nextTree();
    void previousTree();
    void lastTree();
    void delete_instance(){emit signal_delete_instance(this);}
    void on_settings_triggered();


};

#endif // MYTREE_H
