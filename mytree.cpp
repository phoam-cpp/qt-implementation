#include "mytree.h"

#include <Bpp/Phyl/Graphics/PhylogramPlot.h>

MyTree::MyTree(QWidget * parent, vector<Tree*> list):
QWidget(parent){
    this->parent=parent;
    treeList.swap(list);

    connect(this,SIGNAL(signal_delete_instance(QWidget*)),parent,SLOT(delete_instance(QWidget*)));
}

MyTree::~MyTree(){
    delete tree;
    delete canvas;
}

void MyTree::openInWindow(){
    setupWindow();
    loadTree(0);
    window->show();
}

MyTreeCanvas* MyTree::openInCanvas(){
    canvas=new MyTreeCanvas(this);
    loadTree(0);
    return canvas;
}

void MyTree::setupWindow(){
    window = new SecondaryWindow(this);
    canvas = new MyTreeCanvas(this);
    window->setCentralWidget(canvas);
}

void MyTree::loadTree(unsigned long index){

    tree = new TreeTemplate<Node>(*treeList[index]);

    canvas->setTree(tree);

    if(settings.getTreeStyle().toStdString()=="phylogram" && tree->getLeaves()[0]->hasDistanceToFather())
        canvas->setTreeDrawing(*new PhylogramPlot());
    else
        canvas->setTreeDrawing(*new CladogramPlot());

    drawingListener= new MyTreeDrawingListener(canvas);
    canvas->getTreeDrawing()->addTreeDrawingListener(drawingListener);

    canvas->setScene(new QGraphicsScene());

    canvas->setAlignment(Qt::AlignLeft);

    canvas->changeChildsColor(tree->getRootId(),RGBColor(0,0,0));

    setAutoTreeSize();
}

void MyTree::nextTree(){
    if(treeIndex < treeList.size()-1){
        setTreeIndex(treeIndex+1);
    }
}

void MyTree::previousTree(){
    if(treeIndex > 0){
        setTreeIndex(treeIndex-1);
    }
}

void MyTree::lastTree(){
    setTreeIndex(treeList.size()-1);
}

void MyTree::on_settings_triggered(){
    settings.showDialog();
    canvas->setPointSize(settings.getBranchWidth());
    this->setAutoTreeSize();
    loadTree(treeIndex);
}

void MyTree::setTreeIndex(unsigned long index){
    treeIndex=index;
    delete tree;
    tree=new TreeTemplate<Node>(*treeList[treeIndex]);

    drawingListener->setTree(tree);
    canvas->setTree(tree);
    canvas->changeChildsColor(tree->getRootId(),RGBColor(0,0,0));

    setAutoTreeSize();
}

void MyTree::setAutoTreeSize(){
    /*
     * si l'option de taille automatique est activée
    */
    if(settings.isEnableAutoTreeHeight())
        canvas->setZoomH(unsigned(tree->getNumberOfLeaves()*settings.getBoundingBox().height()));
        /*
         * On definie la hauteur en fonction du nombre de feuilles
         * et de la hauteur de la police
        */
    else {
        canvas->setZoomH(settings.getTreeHeight());
        /*
         * sinon on utilise la hauteur par defaut definie par l'utilisateur
        */
    }
    canvas->setZoomW(settings.getTreeWidth());
    canvas->setDrawingSize(canvas->getZoomW(),canvas->getZoomH());
}
