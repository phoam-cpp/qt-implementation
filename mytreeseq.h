#ifndef MYTREESEQ_H
#define MYTREESEQ_H

#include <vector>

#include "secondarywindow.h"
#include "mytree.h"
#include "myseq.h"

#include <QWidget>
#include "Bpp/Phyl/TreeTemplate.h"


using namespace std;
using namespace bpp;

class MyTreeSeq: public QWidget
{
    Q_OBJECT

public:
    MyTreeSeq(QWidget* parent, vector<Tree * >,VectorSequenceContainer*);
    void openInWindow();
    void setupDispay();
    MySeq* getSeq(){return mySeq;}
    MyTree* getTree(){return myTree;}

private:
    vector<Tree*> treeList;
    VectorSequenceContainer* seqList=nullptr;

    SecondaryWindow* window=nullptr;
    QWidget* display=nullptr;
    MyTree* myTree=nullptr;
    MyTreeCanvas* treeCanvas= nullptr;
    MySeq* mySeq=nullptr;

private: signals:
    void pass_add_color();

private slots:
    void add_color(){emit pass_add_color();}
};

#endif // MYTREESEQ_H
