#ifndef MYGRAPHICSVIEW_H
#define MYGRAPHICSVIEW_H

#include <iostream>
#include <algorithm>

#include "mygraphicssimpletextitem.h"

#include "Bpp/Qt/QtTools.h"

#include <QGraphicsView>
#include <QMouseEvent>
#include <QGraphicsColorizeEffect>


using namespace std;
using namespace bpp;

class MyGraphicsView : public QGraphicsView
{
    Q_OBJECT

public:
    MyGraphicsView(QWidget*,string);
    ~MyGraphicsView();
    void mousePressEvent(QMouseEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void setGSeqList(vector<vector<QGraphicsSimpleTextItem *>*> * gSeqList){this->gSeqList=gSeqList;}
    void resetItem(){textItem=nullptr;}

private:
    MyGraphicsSimpleTextItem * textItem=nullptr;//item selectioné
    string alphabet;
    vector<char> allowedChar;//liste des lettres dipo pour un alphabet donné
    vector<vector<QGraphicsSimpleTextItem *>*> * gSeqList;//tableau 2D des items

private: signals:
    void has_changed();
    void delete_item(MyGraphicsSimpleTextItem*);

};

#endif // MYGRAPHICSVIEW_H
