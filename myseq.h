#ifndef MYSEQ_H
#define MYSEQ_H

#include <iostream>
#include <map>
#include <unistd.h>

#include "mycolor.h"
#include "mygraphicssimpletextitem.h"
#include "mygraphicsview.h"
#include "secondarywindow.h"
#include "mysettings.h"
#include "mytreecanvas.h"

#include <QMainWindow>
#include <QObject>
#include <QMenu>
#include <QFileDialog>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGraphicsView>
#include <QGraphicsTextItem>
#include <QMenuBar>
#include <QScrollBar>
#include <QStyleOptionGraphicsItem>
#include <QMessageBox>
#include <QListWidget>
#include <QMouseEvent>
#include <QStatusBar>

#include "Bpp/Seq/Io/Fasta.h"
#include "Bpp/Seq/Alphabet/ProteicAlphabet.h"
#include "Bpp/Qt/QtTools.h"
#include "Bpp/Seq/Alphabet/AlphabetTools.h"
#include "Bpp/Seq/Container/VectorSequenceContainer.h"
#include "Bpp/Phyl/Tree.h"


using namespace std;
using namespace bpp;

#define YSPACING 20;
#define XSPACING 12;
/**
 * @brief The MySeq class provide ...
 */
class MySeq : public QWidget
{

    Q_OBJECT //requis pour utiliser connect(sender,signal,receiver,slot)

public:
    MySeq(VectorSequenceContainer*,MyTreeCanvas* tree=nullptr,QWidget*parent=nullptr);
    ~MySeq();
    /**
     * @brief readFile try to read the file and load the seqList or open an error window
     */
    void readFile();
    /**
     * @brief loadSeq load and show sequences alone or aligned to a tree.
     * @param showColor
     * @return return true or false if an error occured
     */
    bool loadSeq(bool showColor);
    bool loadSeqOnTree(bool showcolor);
    bool isFileOpen(){return fileOpen;}
    void writeSeqName(unsigned long,QPoint,QFont);
    void writeSeq(unsigned long,unsigned long,QPoint,QFont);

    void setupDisplay();
    QGraphicsView * getSeqGraphicsView()const{return graphicsView_seq;}
    void showWindow(){window->show();}

private:
    MyColor colorTool;
    MySettings settings;
    bool fileOpen = false;
    /**
     * @brief tree est un potentiel arbre de reference sur lequel s'aligner
     */
    MyTreeCanvas * treeCanvas=nullptr;
    VectorSequenceContainer * seqList=nullptr;//contiens des sequences
    bool showColor=false;
    string alphabet;

    vector<vector<QGraphicsSimpleTextItem *>*> * gSeqList=nullptr;// tableau 2D des items graphique

    QWidget * parent;
    SecondaryWindow * window=nullptr;
    QMenuBar * menuBar=nullptr;
    QMessageBox * msgBox=nullptr;//msg d'erreur
    QtTools * qtTool = new QtTools();//utile pour gérer la compatibilité entre Qt et autres (ex:string et QString)
    QWidget * display=nullptr;//conteneur principal (central widget de la fenetre)
    MyGraphicsView * graphicsView_seq=nullptr;//conteneur graphic des sequences
    MyGraphicsView * graphicsView_seqName=nullptr;//conteneur graphic des nom de sequences

private: signals:
    void signal_delete_instance(QWidget*);

private slots:
    void add_color(){showColor=!showColor;reload_seq();}//inverse la vaeur de showcolor pour switcher entre les deux mode et recharge les sequences
    void reload_seq(){if(treeCanvas){loadSeqOnTree(showColor);}else{loadSeq(showColor);};}
    void remove_seq(MyGraphicsSimpleTextItem* t){try{seqList->removeSequence(t->text().toStdString()); reload_seq();}catch(Exception){}}
    void save_as();
    void save_change();
    void delete_instance(){emit signal_delete_instance(this);}
    void on_settings_triggered();

};

#endif // MYSEQ_H
