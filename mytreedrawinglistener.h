#ifndef MYTREEDRAWINGLISTENER_H
#define MYTREEDRAWINGLISTENER_H

#include <sstream>

#include <QString>

#include "mytreecanvas.h"
#include "mygraphicssimpletextitem.h"

#include "Bpp/Graphics/GraphicDevice.h"
#include "Bpp/Qt/QtTools.h"

using namespace bpp;
using namespace std;

/**
 * @brief The MyTreeDrawingListener class is an implementation of the TreeDrawingListener interface, used to customize the tree.
 */
class MyTreeDrawingListener : public TreeDrawingListener
{
public:
    MyTreeDrawingListener(MyTreeCanvas*);

    void setTree(Tree* tree){this->tree= new TreeTemplate<Node>(*tree);}

    void beforeDrawNode (const DrawNodeEvent &event)override;
    void afterDrawNode(const DrawNodeEvent &event)override;
    void afterDrawTree(const DrawTreeEvent &event)override{}
    void beforeDrawTree(const DrawTreeEvent &event)override;
    void afterDrawBranch(const DrawBranchEvent &event)override{}
    void beforeDrawBranch(const DrawBranchEvent &event)override;
    TreeDrawingListener* clone() const override{return nullptr;}
    bool isAutonomous() const override{return false;}

    bool isEnabled() const override{return true;}
    void enable(bool tf) override{}

private:
    MyTreeCanvas* canvas;
    TreeTemplate<Node>* tree;
    MySettings settings;

    double minBootstrap=settings.getMinBootstrap();
};

#endif // MYTREEDRAWINGLISTENER_H
