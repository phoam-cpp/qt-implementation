#include "mytreedrawinglistener.h"

MyTreeDrawingListener::MyTreeDrawingListener(MyTreeCanvas * canvas):
TreeDrawingListener()
{
    this->canvas=canvas;
    this->tree=new TreeTemplate<Node>(*canvas->getTree());
}

void MyTreeDrawingListener::beforeDrawTree(const DrawTreeEvent &event){
    //defini la largeur du trait pour dessiner l'arbre
    canvas->getDevice().setCurrentPointSize(canvas->getPointSize());
}

void MyTreeDrawingListener::beforeDrawBranch(const DrawBranchEvent &event){

    //regarde si le père du node lié à l'event a une couleur et l'applique (evite bug de coloration)
    if(canvas->getTree()->getRootId()!=event.getNodeId() &&
            canvas->getTree()->hasNodeProperty(canvas->getTree()->getFatherId(event.getNodeId()),"color")){

        RGBColor * color = const_cast<RGBColor*>(
                    reinterpret_cast<const RGBColor *>(
                        canvas->getTree()->getNodeProperty(
                            canvas->getTree()->getFatherId(event.getNodeId()),"color")));

        canvas->getDevice().setCurrentForegroundColor(*color);
    }
}

void MyTreeDrawingListener::beforeDrawNode(const DrawNodeEvent &event){
    RGBColor color = canvas->getDevice().getCurrentForegroundColor();

    //remet la couleur en noir pour écrire la valeur de bootstrap et l'écrit si besoins et si disponible
    canvas->getDevice().setCurrentForegroundColor(RGBColor(0,0,0));
    if(canvas->getShowBootstrap() && tree->getNode(event.getNodeId())->hasBootstrapValue()){

        double bootstrapValue = tree->getNode(event.getNodeId())->getBootstrapValue();

        if(bootstrapValue >= settings.getMinBootstrap()){

            //Convert double into string
            std::ostringstream strs;
            strs << bootstrapValue;
            std::string str = strs.str();

            canvas->getDevice().drawText(event.getCursor().getX(),event.getCursor().getY(),str,
                                         GraphicDevice::TEXT_HORIZONTAL_LEFT,GraphicDevice::TEXT_VERTICAL_CENTER);
        }
    }
    canvas->getDevice().setCurrentForegroundColor(color);

    //ecrit le nom des feuilles
    if(canvas->getTree()->isLeaf(event.getNodeId())){
        canvas->getDevice().setCurrentForegroundColor(RGBColor(0,0,0));
        MyColor colorTool;
        QtTools qtTool;
        MyGraphicsSimpleTextItem * leafName = new MyGraphicsSimpleTextItem(
                    qtTool.toQt(tree->getNode(event.getNodeId())->getName()),true);
        leafName->setPos(event.getCursor().getX()+10,event.getCursor().getY()-settings.getBoundingBox().height()/2);
        leafName->setFlag(QGraphicsItem::ItemIsSelectable, true);
        leafName->setFont(QFont("Courier",settings.getFontSize()));
        canvas->getDevice().getScene().addItem(leafName);
    }
}

void MyTreeDrawingListener::afterDrawNode(const DrawNodeEvent &event){
    //permet de colorer les noeuds des feuilles
    if(canvas->getTree()->getRootId()!=event.getNodeId() &&
            canvas->getTree()->hasNodeProperty(canvas->getTree()->getFatherId(event.getNodeId()),"color")){

        RGBColor * color = const_cast<RGBColor*>(
                    reinterpret_cast<const RGBColor *>(
                        canvas->getTree()->getNodeProperty(event.getNodeId(),"color")));

        canvas->getDevice().setCurrentForegroundColor(*color);
    }

    //dessine le noeud
    canvas->getDevice().drawRect(event.getCursor().getX()-2,event.getCursor().getY()-2,4,4);
}
