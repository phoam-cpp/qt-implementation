#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <iostream>
#include <string>
#include <QMainWindow>

#include "myseq.h"
#include "mytree.h"
#include "mysettings.h"
#include "mytreeseq.h"

#include <QFileDialog>

#include "Bpp/Phyl/Io/Newick.h"

using namespace std;
using namespace bpp;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT //requis pour utiliser connect(sender,signal,receiver,slot)

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override;
     void closeEvent(QCloseEvent *) override;

     void formatTreeFile(string);

     VectorSequenceContainer* readSeqFile();

private slots:

    void on_openTreeAction_triggered();
    void on_actionSettings_triggered();
    void on_openTreeButton_clicked(){on_openTreeAction_triggered();}
    void on_openSeqAction_triggered();
    void on_openSeqButton_clicked(){on_openSeqAction_triggered();}
    void on_openTreeSeqAction_triggered();
    void on_openTreeSeqButton_clicked(){on_openTreeSeqAction_triggered();}

    void delete_instance(QWidget* w){delete w;windowCount--;}

private:
    Ui::MainWindow * ui;
    MySettings settings;

    unsigned int windowCount=0;
};



#endif // MAINWINDOW_H
