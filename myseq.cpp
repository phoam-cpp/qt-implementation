﻿#include "myseq.h"

/******************MySeq*********************************/
MySeq::MySeq(VectorSequenceContainer * seqList, MyTreeCanvas* treeCanvas,QWidget * parent) :
    QWidget(parent){
    this->parent=parent;
    this->seqList=seqList;
    this->treeCanvas=treeCanvas;//si il y a un arbre, le prend en reference pour alignement
    setupDisplay();

    connect(this,SIGNAL(signal_delete_instance(QWidget*)),this->parent,SLOT(delete_instance(QWidget*)));

    bool ok;
    if(treeCanvas)
        ok = loadSeqOnTree(false);
    else
        ok = loadSeq(false);

    if(!ok){
        delete msgBox;
        msgBox = new QMessageBox();
        msgBox->setText("ERROR\nThe sequence file selected is incomplete or does not match.");
        msgBox->setIcon(QMessageBox::Icon::Warning);
        msgBox->exec();
        fileOpen = false;
    }
    else {
        fileOpen = true;
        if(!treeCanvas){
            showWindow();
        }
    }


}

MySeq::~MySeq(){
    delete msgBox;
    delete seqList;
    delete menuBar;
    delete graphicsView_seq;
    delete graphicsView_seqName;
    delete display;
}

bool MySeq::loadSeq(bool showColor){
    int x=0,y=0;
    QFont myFont("Courier",settings.getFontSize());

    gSeqList = new vector<vector<QGraphicsSimpleTextItem *>*>();

    delete graphicsView_seq->scene();
    graphicsView_seq->setScene(new QGraphicsScene(graphicsView_seq));
    delete graphicsView_seqName->scene();
    graphicsView_seqName->setScene(new QGraphicsScene(graphicsView_seqName));

    for (unsigned long seqIndex=0; seqIndex<unsigned(seqList->getNumberOfSequences());seqIndex++) {
        gSeqList->insert(gSeqList->end(),new vector<QGraphicsSimpleTextItem *>());
        writeSeqName(seqIndex,QPoint(x,y),myFont);

        for(unsigned long nuclIndex=0;nuclIndex<seqList->getSequence(seqIndex).size();nuclIndex++){

            writeSeq(seqIndex,nuclIndex,QPoint(x,y),myFont);

            x+=settings.getBoundingBox().width();
        }
        y+=settings.getBoundingBox().height();
        x=0;
    }
    graphicsView_seq->setGSeqList(gSeqList);
    return true;
}

bool MySeq::loadSeqOnTree(bool showColor){
    int x=0,y=0;
    QFont myFont("Courier",settings.getFontSize());

    gSeqList = new vector<vector<QGraphicsSimpleTextItem *>*>();

    delete graphicsView_seq->scene();
    graphicsView_seq->setScene(new QGraphicsScene(graphicsView_seq));

    for (unsigned long leafIndex=0; leafIndex<unsigned(treeCanvas->getTree()->getLeavesNames().size());leafIndex++) {
        if(treeCanvas->isNodeCollapsed(treeCanvas->getTree()->getLeafId(treeCanvas->getTree()->getLeavesNames()[leafIndex])))
            continue;
        gSeqList->insert(gSeqList->end(),new vector<QGraphicsSimpleTextItem *>());
        writeSeqName(leafIndex,QPoint(x,y),myFont);
         for(unsigned long nuclIndex=0;nuclIndex<seqList->getSequence(treeCanvas->getTree()->getLeavesNames()[leafIndex]).size();nuclIndex++){
             if(!seqList->hasSequence(treeCanvas->getTree()->getLeavesNames()[leafIndex]))
                 return false;


             writeSeq(leafIndex,nuclIndex,QPoint(x,treeCanvas->getTreeDrawing()->
                                 getNodePosition(treeCanvas->getTree()->
                                                 getLeafId(treeCanvas->getTree()->
                                                           getLeavesNames()[leafIndex])).getY()),myFont);
             x+=settings.getBoundingBox().width();
         }
         y+=settings.getBoundingBox().height();
         x=0;
    }
    graphicsView_seq->setGSeqList(gSeqList);
    return true;
}

void MySeq::writeSeqName(unsigned long seqIndex,QPoint pos, QFont font){
    MyGraphicsSimpleTextItem * seqName;
    seqName = new MyGraphicsSimpleTextItem(qtTool->toQt(seqList->getSequence(seqIndex).getName()));
    seqName->setPos(pos);
    seqName->setFlag(QGraphicsItem::ItemIsSelectable, true);
    seqName->setFont(font);
    if(!treeCanvas)
        graphicsView_seqName->scene()->addItem(seqName);
    gSeqList->at(seqIndex)->insert(gSeqList->at(seqIndex)->end(),seqName);
}

void MySeq::writeSeq(unsigned long seqIndex,unsigned long nuclIndex, QPoint pos, QFont font){
    MyGraphicsSimpleTextItem * nucl;
    if(treeCanvas)
        nucl = new MyGraphicsSimpleTextItem(qtTool->toQt(seqList->getSequence(treeCanvas->getTree()->getLeavesNames()[seqIndex]).getChar(nuclIndex)),
                                            showColor,seqList->getAlphabet()->getAlphabetType(),QPoint(nuclIndex,seqIndex));
    else
        nucl = new MyGraphicsSimpleTextItem(qtTool->toQt(seqList->getSequence(seqIndex).getChar(nuclIndex)),
                                        showColor,seqList->getAlphabet()->getAlphabetType(),QPoint(nuclIndex,seqIndex));

    gSeqList->at(seqIndex)->insert(gSeqList->at(seqIndex)->end(),nucl);
    nucl->setColorTool(&colorTool);
    nucl->setFlag(QGraphicsItem::ItemIsSelectable, true);
    nucl->setFont(font);
    nucl->setPos(pos);
    graphicsView_seq->scene()->addItem(nucl);
    graphicsView_seq->resetItem();
}

void MySeq::setupDisplay(){
    if(!treeCanvas){
        window = new SecondaryWindow(this);

        display = new QWidget();
        window->setCentralWidget(display);

        /*Creation des QGraphicsView*/
        graphicsView_seqName = new MyGraphicsView(this,seqList->getAlphabet()->getAlphabetType());
        graphicsView_seqName->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        graphicsView_seqName->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        graphicsView_seqName->setScene(new QGraphicsScene(graphicsView_seqName));
        graphicsView_seqName->setAlignment(Qt::AlignRight|Qt::AlignTop);
        QSizePolicy spName(QSizePolicy::Preferred, QSizePolicy::Preferred);
        spName.setHorizontalStretch(1);
        graphicsView_seqName->setSizePolicy(spName);
    }

    graphicsView_seq = new MyGraphicsView(this,seqList->getAlphabet()->getAlphabetType());
    graphicsView_seq->setHorizontalScrollBarPolicy ( Qt::ScrollBarAlwaysOn );
    graphicsView_seq->setScene(new QGraphicsScene(graphicsView_seq));

    if(treeCanvas)
        graphicsView_seq->setAlignment(Qt::AlignLeft);
    else
        graphicsView_seq->setAlignment(Qt::AlignLeft|Qt::AlignTop);

    graphicsView_seq->setDragMode(QGraphicsView::RubberBandDrag);
    graphicsView_seq->setRenderHint(QPainter::Antialiasing, false);
    QSizePolicy spSeq(QSizePolicy::Preferred, QSizePolicy::Preferred);
    spSeq.setHorizontalStretch(7);
    graphicsView_seq->setSizePolicy(spSeq);

    if(treeCanvas){
        connect(graphicsView_seq->verticalScrollBar(), SIGNAL(valueChanged(int)),
                treeCanvas->verticalScrollBar(), SLOT(setValue(int)));
        connect(treeCanvas->verticalScrollBar(), SIGNAL(valueChanged(int)),
                graphicsView_seq->verticalScrollBar(), SLOT(setValue(int)));
    }
    else{
        /*Connection d'une scroll bar a l'autre*/
        connect(graphicsView_seq->verticalScrollBar(), SIGNAL(valueChanged(int)),
                graphicsView_seqName->verticalScrollBar(), SLOT(setValue(int)));
        connect(graphicsView_seqName->verticalScrollBar(), SIGNAL(valueChanged(int)),
                graphicsView_seq->verticalScrollBar(), SLOT(setValue(int)));

        /*Ajout des QGraphicsView au layout*/
        display->setLayout(new QHBoxLayout(display));
        display->layout()->addWidget(graphicsView_seqName);
        display->layout()->addWidget(graphicsView_seq);
    }

    connect(graphicsView_seq,SIGNAL(has_changed()),this,SLOT(save_change()));
}

void MySeq::save_as(){
    QString fileName = QFileDialog::getOpenFileName(this,"Save as ?","");
    Fasta fastaTools;
    string seq = "";
    if(fileName.toStdString()!=""){
        for (unsigned long i=0;i<gSeqList->size();i++) {
            for (unsigned long j = 1; j<gSeqList->at(i)->size();j++){
                seq+=gSeqList->at(i)->at(j)->text().toStdString();
            }
            try{
                BasicSequence * sequence = new BasicSequence(gSeqList->at(i)->at(0)->text().toStdString(),seq,new DNA());
                seqList->setSequence(gSeqList->at(i)->at(0)->text().toStdString(),*sequence);
            }
            catch(Exception){
                try{
                    BasicSequence * sequence = new BasicSequence(gSeqList->at(i)->at(0)->text().toStdString(),seq,new RNA());
                    seqList->setSequence(gSeqList->at(i)->at(0)->text().toStdString(),*sequence);
                }
                catch(Exception){
                    try{
                        BasicSequence * sequence = new BasicSequence(gSeqList->at(i)->at(0)->text().toStdString(),seq,new ProteicAlphabet());
                        seqList->setSequence(gSeqList->at(i)->at(0)->text().toStdString(),*sequence);
                    }
                    catch(Exception){
                    }
                }
            }
            seq="";
        }
        fastaTools.writeSequences(fileName.toStdString(),*seqList);
    }
}

void MySeq::save_change(){
    string seq = "";

    for (unsigned long i=0;i<gSeqList->size();i++) {
        for (unsigned long j = 1; j<gSeqList->at(i)->size();j++){
            seq+=gSeqList->at(i)->at(j)->text().toStdString();
        }
        try{
            BasicSequence * sequence = new BasicSequence(gSeqList->at(i)->at(0)->text().toStdString(),seq,new DNA());
            seqList->setSequence(gSeqList->at(i)->at(0)->text().toStdString(),*sequence);
        }
        catch(Exception){
            try{
                BasicSequence * sequence = new BasicSequence(gSeqList->at(i)->at(0)->text().toStdString(),seq,new RNA());
                seqList->setSequence(gSeqList->at(i)->at(0)->text().toStdString(),*sequence);
            }
            catch(Exception){
                try{
                    BasicSequence * sequence = new BasicSequence(gSeqList->at(i)->at(0)->text().toStdString(),seq,new ProteicAlphabet());
                    seqList->setSequence(gSeqList->at(i)->at(0)->text().toStdString(),*sequence);
                }
                catch(Exception){
                }
            }
        }
        seq="";
    }
}

void MySeq::on_settings_triggered(){
    settings.showDialog();
    loadSeq(showColor);
}
