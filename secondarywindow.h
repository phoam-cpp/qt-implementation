#ifndef SECONDARYWINDOW_H
#define SECONDARYWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QStatusBar>
#include <QMenuBar>
#include <QMenu>

using namespace std;

class SecondaryWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit SecondaryWindow(QWidget *parent = nullptr);
    void closeEvent(QCloseEvent *event);
    void setupWindow();

private:
    QWidget* parent=nullptr;

signals:
    void windowClosed();
public slots:
};

#endif // SECONDARYWINDOW_H
