#include "secondarywindow.h"

SecondaryWindow::SecondaryWindow(QWidget *parent):
QMainWindow(parent)
{
    this->parent=parent;
    setupWindow();
    connect(this,SIGNAL(windowClosed()),parent,SLOT(delete_instance()));
}

void SecondaryWindow::closeEvent(QCloseEvent *event){
    emit windowClosed();
}

void SecondaryWindow::setupWindow(){
    this->setMenuBar(new QMenuBar());

    QMenu * file = new QMenu("File");

        QAction * add_Seq = new QAction("Add sequences");
        file->addAction(add_Seq);

        QAction * save_as = new QAction("Save as");
        connect(save_as,SIGNAL(triggered()),parent,SLOT(save_as()));
        file->addAction(save_as);

    this->menuBar()->addMenu(file);

    QMenu * edit = new QMenu("Edit");

        QAction * settings = new QAction("Settings");
        connect(settings,SIGNAL(triggered()),parent,SLOT(on_settings_triggered()));

        QAction * color = new QAction("Add color");
        edit->addAction(color);
        connect(color,SIGNAL(triggered()),parent,SLOT(add_color()));

    edit->addAction(settings);
    this->menuBar()->addMenu(edit);

    this->setStatusBar(new QStatusBar());
}
