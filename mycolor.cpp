#include "mycolor.h"

MyColor::MyColor(){

    nuclToColor["A"]=QColor(50,50,230);
    nuclToColor["T"]=QColor(230,230,50);
    nuclToColor["U"]=QColor(230,230,50);
    nuclToColor["G"]=QColor( 50,230,50);
    nuclToColor["C"]=QColor(230,50,50);
    nuclToColor["-"]=QColor(230,230,230);
    nuclToColor[""]=QColor(230,230,230);

    aminoToColor["A"]=QColor(200,200,200);
    aminoToColor["C"]=QColor(230,230,  0);
    aminoToColor["D"]=QColor(230,230, 10);
    aminoToColor["E"]=QColor(230,230, 10);
    aminoToColor["F"]=QColor( 50, 50,170);
    aminoToColor["G"]=QColor(235,235,235);
    aminoToColor["H"]=QColor(130,130,210);
    aminoToColor["I"]=QColor( 15,130, 15);
    aminoToColor["K"]=QColor( 20, 90,255);
    aminoToColor["L"]=QColor( 15,130, 15);
    aminoToColor["M"]=QColor(230,230,  0);
    aminoToColor["N"]=QColor(  0,220,220);
    aminoToColor["P"]=QColor(220,150,130);
    aminoToColor["Q"]=QColor(  0,220,220);
    aminoToColor["R"]=QColor( 20, 90,255);
    aminoToColor["S"]=QColor(250,150,  0);
    aminoToColor["T"]=QColor(250,150,  0);
    aminoToColor["V"]=QColor( 15,130, 15);
    aminoToColor["W"]=QColor(180, 90,180);
    aminoToColor["Y"]=QColor( 50, 50,170);
    aminoToColor["-"]=QColor(190,160,110);
    aminoToColor[""]=QColor(190,160,110);

}

MyColor::~MyColor(){}
