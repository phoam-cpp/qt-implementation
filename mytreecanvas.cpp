#include "mytreecanvas.h"

#include "Bpp/Graphics/RgbColor.h"

MyTreeCanvas::MyTreeCanvas(QWidget *parent) :
    TreeCanvas(parent){
    this->parent = parent;
    tSB = new TreeStatisticsBox();
//    tCC = new TreeCanvasControlers();
//    tCC->setTreeCanvas(this);
//    tCC->actualizeOptions();
//    tCC->addActionListener(this);
//    tCC->applyOptions(this);

    connect(this,SIGNAL(nextTree()),parent,SLOT(nextTree()));
    connect(this,SIGNAL(previousTree()),parent,SLOT(previousTree()));
    connect(this,SIGNAL(lastTree()),parent,SLOT(lastTree()));
}

MyTreeCanvas::~MyTreeCanvas(){
    delete tSB;
//    delete tCC;
}

void MyTreeCanvas::setTree(const Tree* tree)
{
    this->tree=new TreeTemplate<Node>(*tree);
    this->getTreeDrawing()->setTree(tree);
    TreeCanvas::setTree(tree);
}

void MyTreeCanvas::showContextMenu(bool hasNode,QPoint pos){
    QMenu contextMenu;
        QAction swap("Swap");
            connect(&swap, SIGNAL(triggered()), this, SLOT(swap_triggered()));
        contextMenu.addAction(&swap);
        QAction collapse("Hide/Show");
            connect(&collapse, SIGNAL(triggered()), this, SLOT(collapse_triggered()));
        contextMenu.addAction(&collapse);
        QAction reroot("Reroot");
            connect(&reroot, SIGNAL(triggered()),this,SLOT(reroot_triggered()));
        contextMenu.addAction(&reroot);
        QAction showBootstrap("show bootstrap value");
            connect(&showBootstrap, SIGNAL(triggered()),this,SLOT(showBootstrap_triggered()));
        contextMenu.addAction(&showBootstrap);

        QMenu branchWidth("branch width");
            QAction increaseWidth("+1px");
                connect(&increaseWidth,SIGNAL(triggered()),this,SLOT(increaseWidth_triggered()));
            branchWidth.addAction(&increaseWidth);
            QAction decreaseWidth("-1px");
                connect(&decreaseWidth,SIGNAL(triggered()),this,SLOT(decreaseWidth_triggered()));
            branchWidth.addAction(&decreaseWidth);
        contextMenu.addMenu(&branchWidth);

        QMenu color("Color");
            QAction black("black(default)");
                connect(&black,SIGNAL(triggered()),this,SLOT(colorBlack_triggered()));
            color.addAction(&black);
            QAction customColor("custom...");
                connect(&customColor,SIGNAL(triggered()),this,SLOT(colorCustom_triggered()));
            color.addAction(&customColor);
            QAction red("red");
                connect(&red,SIGNAL(triggered()),this,SLOT(colorRed_triggered()));
            color.addAction(&red);
            QAction green("green");
                connect(&green,SIGNAL(triggered()),this,SLOT(colorGreen_triggered()));
            color.addAction(&green);
            QAction blue("blue");
                connect(&blue,SIGNAL(triggered()),this,SLOT(colorBlue_triggered()));
            color.addAction(&blue);
        contextMenu.addMenu(&color);

        if(!hasNode){
            swap.setEnabled(false);
            collapse.setEnabled(false);
            reroot.setEnabled(false);
            color.setEnabled(false);
        }

        contextMenu.exec(pos);
}

void MyTreeCanvas::controlerTakesAction(){
    redraw();
}

void MyTreeCanvas::mousePressEvent(QMouseEvent * event)
{
    NodeMouseEvent nME(*this,*event);

    if(nME.hasNodeId())
        selectedNode = nME.getNodeId();

    if(event->button()==Qt::RightButton){
        if(nME.hasNodeId()){
            showContextMenu(true,event->globalPos());
        }
        else {
            showContextMenu(false,event->globalPos());
        }
    }

}

void MyTreeCanvas::wheelEvent(QWheelEvent *event){
    if(wheelEventEnable==false)
        return;

    if(ctrlPressed==true){
        if(event->delta()<0 && zoomHeight*0.75>=100){
            zoomHeight*=0.75;
        }
        else if(event->delta()>0){
            zoomHeight*=1.25;
        }
        setDrawingSize(getZoomW(),getZoomH());
    }
    if(altPressed==true){
        if(event->delta()<0 && zoomWidth*0.75>=100){
            zoomWidth*=0.75;
        }
        else if(event->delta()>0){
            zoomWidth*=1.25;
        }
        setDrawingSize(getZoomW(),getZoomH());
    }
    else if(ctrlPressed==false && altPressed==false){
        TreeCanvas::wheelEvent(event);
    }
    emit hasChanged();
}

void MyTreeCanvas::keyPressEvent(QKeyEvent * event){
    if(event->key()==Qt::Key_Right)
        emit nextTree();
    if(event->key()==Qt::Key_Left)
        emit previousTree();
    if(event->key()==Qt::Key_Up)
        emit lastTree();
    if(event->key()==Qt::Key_Down)
        emit firstTree();
    if(event->key()==Qt::Key_Control)
        ctrlPressed=true;
    if(event->key()==Qt::Key_Alt)
        altPressed=true;
    emit hasChanged();
    //MyTreeCanvas::keyPressEvent(event);
}

void MyTreeCanvas::keyReleaseEvent(QKeyEvent * event){
    if(event->key()==Qt::Key_Control)
        ctrlPressed=false;
    if(event->key()==Qt::Key_Alt)
        altPressed=false;
}

void MyTreeCanvas::changeInTree(int i){

    int selectedNode = getSelectedNode();
    switch (i) {
    case 1:
        if(!tree->getNode(selectedNode)->isLeaf()){
            tree->swapNodes(selectedNode,0,1);
            getTreeDrawing()->setTree(tree);
        }
        break;
    case 2:
        if(selectedNode!=tree->getRootId()){
            collapseNode(selectedNode,!isNodeCollapsed(selectedNode));
        }
        break;
    case 3:
        if(tree->getNode(selectedNode)->hasFather()){
            tree->rootAt(selectedNode);
            getTreeDrawing()->setTree(tree);
        }
        break;
    default:
        break;
    }

    redraw();
    emit hasChanged();
}

void MyTreeCanvas::changeChildsColor(int nodeId,RGBColor color){
    //Change la couleur du node actuel
    tree->getNode(nodeId)->setNodeProperty("color",color);

    //initialise i à 0
    unsigned long i=0;

    //tant que le noeud actuel à des enfants on lui applique cette fonction
    while(i<tree->getNode(nodeId)->getSons().size()){
        changeChildsColor(tree->getSonsId(nodeId)[i],color);
        i++;
    }

    /*la recursivité prend fin quand le noeud enfant est une feuille,
    il n'a donc pas d'enfants et ne rentre pas dans le while*/
}

void MyTreeCanvas::changeColor(RGBColor color){
    tree->getNode(getSelectedNode())->setNodeProperty("color",color);
    changeChildsColor(getSelectedNode(),color);
    this->setTree(tree);
}
