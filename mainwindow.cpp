﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

using namespace std;
using namespace bpp;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event){

    if(windowCount>0){
        event->ignore();

        QDialog closeDialog;
        QVBoxLayout layout;

        QLabel quitText("Do you really want to leave?\nAll unsaved changes will be lost!");
        layout.addWidget(&quitText);

        QDialogButtonBox buttonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
        buttonBox.connect(&buttonBox, &QDialogButtonBox::accepted, &closeDialog, &QDialog::accept);
        buttonBox.connect(&buttonBox, &QDialogButtonBox::rejected, &closeDialog, &QDialog::reject);
        layout.addWidget(&buttonBox);

        closeDialog.setLayout(&layout);

        closeDialog.setFixedSize(QSize(300,100));

        closeDialog.exec();
        if(closeDialog.result()){
            QMainWindow::closeEvent(event);
        }
    }
}

void MainWindow::on_actionSettings_triggered(){
    //settings = new MySettings();
    settings.showDialog();
    //delete settings;
}

void MainWindow::on_openTreeAction_triggered(){
    //appel une instance de visualisation d'arbre

    QString fileName="";
    fileName = QFileDialog::getOpenFileName(this,"Open Tree File","", "Tree Files (*.tre *.nw)");

    formatTreeFile(fileName.toStdString());

    Newick nwTools;
    if(fileName.toStdString()!=""){
        vector<Tree * > treeList;
        nwTools.read(fileName.toStdString(), treeList);
        if(treeList.size()>0){
            MyTree * treeTool = new MyTree(this,treeList);
            treeTool->openInWindow();
            windowCount++;
        }
    }
}

void MainWindow::on_openSeqAction_triggered(){
    //appel une instance de visualisation de sequence
    VectorSequenceContainer * seqList = readSeqFile();
    MySeq * seqTool;
    if(seqList==nullptr){
        return;
    }else if(seqList->getNumberOfSequences()>0){
        seqTool = new MySeq(seqList,nullptr,this);
        windowCount++;
    }
}

VectorSequenceContainer * MainWindow::readSeqFile(){
    QString fileName="";

    fileName = QFileDialog::getOpenFileName(this,"Open Seq File",
                                            "", "Seq Files (*.fas *.fa *.fasta)");
    Fasta fastaTools;
    if(fileName.toStdString()!=""){
        try{
            VectorSequenceContainer * seqList = new VectorSequenceContainer(new DNA());
            fastaTools.readSequences(fileName.toStdString(),*seqList);
            string alphabet="DNA";
            return seqList;
        }
        catch(Exception){
            try{
                VectorSequenceContainer * seqList = new VectorSequenceContainer(new RNA());
                fastaTools.readSequences(fileName.toStdString(),*seqList);
                string alphabet="RNA";
                return seqList;
            }
            catch(Exception){
                try{
                    VectorSequenceContainer * seqList = new VectorSequenceContainer(new ProteicAlphabet());
                    fastaTools.readSequences(fileName.toStdString(),*seqList);
                    string alphabet="ProteicAlphabet";
                    return seqList;
                }
                catch(Exception){
                }
            }
        }
    }
    return nullptr;
}

void MainWindow::on_openTreeSeqAction_triggered(){

    QString fileName="";
    fileName = QFileDialog::getOpenFileName(this,"Open Tree File","", "Tree Files (*.tre *.nw)");
    Newick nwTools;
    if(fileName.toStdString()!=""){
        vector<Tree * > treeList;
        nwTools.read(fileName.toStdString(), treeList);

        VectorSequenceContainer * seqList = readSeqFile();

        if(seqList==nullptr)
            return;

        MyTreeSeq* treeseq = new MyTreeSeq(this,treeList,seqList);
        if(treeseq->getSeq()->isFileOpen())
            treeseq->openInWindow();
        else {
            delete treeseq;
        }
    }
}

void MainWindow::formatTreeFile(string filename){
    int caract=0;
    int caractPrec=0;
    FILE* file;
    FILE* temp;
    file = fopen(filename.c_str(),"r+");
    temp = fopen("data/temp.nw","w+");
    if(file == NULL || temp == NULL){
        cout << "ERREUR fichier temp" << endl;
        return;
    }
    while((caract = fgetc(file)) != EOF){

        if(caractPrec == ';' && caract != '\n'){
            fputc('\n',temp);
        }

        fputc(caract, temp);

        caractPrec = caract;
    }
    freopen(filename.c_str(),"w+", file);
    fseek(temp,0,SEEK_SET);
    while((caract = fgetc(temp)) != EOF){
        fputc(caract, file);
    }

    fclose(file);
    fclose(temp);
}
